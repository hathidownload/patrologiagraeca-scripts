#!/bin/sh
usage="$0 dir"
test $# -ne 1 && echo "$usage" && exit 1

# move volume subdirectories out of catalogue directories
cd "$1"
find * -type d | awk -F / 'NF==2 {print $0}' | while read i
do
	b=`basename "$i"`
	mkdir -p "$b"
	cp "$i"/* "$b/"
done

# remove leading t. and v. from directory names
for i in *
do
	b=`echo "$i" | sed 's/^v\.//;s/^t\.//;'`
	mkdir -p "$b"
	mv "$i"/* "$b"
done
rmdir *

# remove leading spaces from directory names
for i in *
do
	b=`echo "$i" | sed 's/^ //g'`
	test "$i" != "$b" && mv "$i" "$b"
done

# remove leading vol. from directory names
for i in *
do
	b=`echo "$i" | sed 's/^vol. //g'`
	test "$i" != "$b" && mv "$i"/* "$b" && rmdir "$i"
done

# sort pg specific things
mv unlabelled/uva.x030486937.pdf 76/
mv unlabelled/uva.x030486936.pdf 75/
