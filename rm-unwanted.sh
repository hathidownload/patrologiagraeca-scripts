#!/bin/sh
usage="$0 dir list-of-pdfs"
test $# -ne 2 && echo "$usage" && exit 1

list=`readlink -f "$2"`

cd "$1"
while read i
do
	# ignore comment lines
	first=`echo "$i" | cut -c 1`
	test "$first" == "#" && continue

	rm */"$i"
	# remove any newly empty directories
	rmdir * 2>/dev/null
done < "$list"
