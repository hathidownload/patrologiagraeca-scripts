#!/bin/sh
usage="$0 bibdir"

test $# -ne 1 && echo "$usage" && exit 1

mkdir -p "$1"

for i in 009728725 011986699 001931731 007035210 008394160 011241341 008882185 008699447 011530216
do
	curl "http://catalog.hathitrust.org/Search/SearchExport?handpicked=$i&method=ris" > "$1/$i.refer"
done
